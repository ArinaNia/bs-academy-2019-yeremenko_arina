const assert = require('assert');
const {URL} = require('url');
const path = require('path');
const expect = require('chai').expect;

function login () {
    const emailField = $('input[name=email]');
    const passField = $('input[type=password]');
    const loginButton = $('button.button.is-primary');

    emailField.setValue('rinasaara@gmail.com');
    passField.setValue('Qwerty1234');
    loginButton.click();


    const headerDropDown = $('div.profile.navbar-link.navbar-dropdown-menu');
    expect($('.navbar-end').isDisplayed());
    headerDropDown.waitForDisplayed(10000);
}

describe('test hedonist', () => {

    // beforeEach(function() {
    //     browser.url('https://staging.bsa-hedonist.online');
    //     $('#app').waitForDisplayed();
    // });

    xit('log in hedonist', () => {

        login();
        const url= new URL(browser.getUrl());
        const actualUrl = url.hostname.toString() + url.pathname.toString();
        assert.equal(actualUrl, "staging.bsa-hedonist.online/search");
    });

    xit('registering in hedonist', () => {
        browser.url('https://staging.bsa-hedonist.online/signup');

        const firstNameField = $('input[name=firstName]');
        const lastNameField = $('input[name=lastName]');
        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');

        firstNameField.setValue('Tina');
        lastNameField.setValue('Chan');
        emailField.setValue('rinasaara100@gmail.com');
        passField.setValue('Qwerty1234');
        loginButton.click();

        browser.pause(5000);

        const url= new URL(browser.getUrl());
        const actualUrl = url.hostname.toString() + url.pathname.toString();
        assert.equal(actualUrl, "staging.bsa-hedonist.online/login")
    });
    xit('log in hedonist with invalid data', () => {

        browser.url('https://staging.bsa-hedonist.online');

        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');

        emailField.setValue('qwertty@gma.com');
        passField.setValue('Qwerty1234');
        loginButton.click();

        browser.pause(5000);

    });

    xit('create new place', () => {
        login();
        browser.url('https://staging.bsa-hedonist.online/places/add');

        const namePlaceField  = $('input.input.is-medium');
        const zipField = $('input[placeholder="09678"]');
        const adressField = $('input[placeholder="Khreschatyk St., 14"]');
        const phoneField = $('input[placeholder="+380961112233"]');
        const siteField = $('input[placeholder="http://the-best-place.com/"]');
        const descriptionField = $('textarea.textarea');
        const buttonNext = $('#app > div.section > div > div > section > div:nth-child(1) > div.buttons.is-centered > span');

        namePlaceField.setValue('Name ' + Math.round(Math.random() * 500));
        zipField.setValue('09678');
        adressField.setValue('Test adress');
        phoneField.setValue('+38909994949444');
        siteField.setValue('https://study.binary-studio.com/');
        descriptionField.setValue('We are the Best place, that you could visit..');
        buttonNext.click();

        const uploadControl = $('input[type=file]');

        browser.pause(5000);

        let arrayPicture = ['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.png'];
        for (let i = 0; i < arrayPicture.length; i++) {
            const filePath = path.join(__dirname, arrayPicture[i]);
            uploadControl.setValue(filePath);
        }

        browser.pause(5000);

        $$('span.button.is-success')[1].click();
        $$('span.button.is-success')[2].click();


        $('select').options.selectedIndex = 1;
        $('#app > div.section > div > div > section > div:nth-child(4) > div.tab-wrp > div:nth-child(2) > div > div > div > span > select').options.selectedIndex = 1;

        browser.pause(5000);

        $$('span.button.is-success')[3].click();


        $('select').click();
        const selectOption = $$('select > option')[1];
        $('select > option').selectByVisibleText("Beer");
        $$('select')[1].click();
        $('select > option').selectByVisibleText("Kneipp");


        document.querySelectorAll('#app > div.section > div > div > section > div:nth-child(5) > div.columns.is-centered.column-feature > div > div:nth-child(1) > div.level-right > label')[0].click();
        // '#app > div.section > div > div > section > div:nth-child(5) > div.columns.is-centered.column-feature > div > div:nth-child(1) > div.level-right > label'.checked = true;
        $$('span.button.is-success')[4].click();

        $$('span.button.is-success')[5].click();

        $('#app > div.section > div > div > section > div:nth-child(7) > div > div.buttons.is-centered > span.button.is-success').click();

        const url = new URL(browser.getUrl());
        const actualUrl = url.host.toString() + url.pathname.toString();

        assert(actualUrl.includes('/places'));

    });
    xit('create new list', () => {
        login();
        browser.url('https://staging.bsa-hedonist.online/my-lists/add');

        const listNameField = $('#list-name');
        const buttonSaveList = $('button.is-success');
        const searchPlaceField = $('input.search-field.input');
        const choicePlaceLI = $('div.search-places__list > ul > li');
        const uploadControl = $('#app > div.container > form > div.image-upload > div:nth-child(2) > div > label > input');
        const NameList = 'Name list' + ' ' + Math.round(Math.random() * 500);
        let arrayImage = ['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.png'];
        const randomImage = Math.floor(Math.random() * arrayImage.length);

        browser.pause(5000);

        const filePath = path.join(__dirname, arrayImage[randomImage]);
        uploadControl.setValue(filePath);
        browser.pause(5000);

        listNameField.setValue(NameList);
        searchPlaceField.setValue('Red Cat');
        choicePlaceLI.click();

        buttonSaveList.click();
        assert.equal($$('.title.has-text-primary')[0].getText(), NameList)
    });
    xit('delete list', () => {
        login();
        browser.url('https://staging.bsa-hedonist.online/my-lists');
        const mainContainer = $('section.container');

        browser.pause(5000);

        const arrayLength = $$('div.container.place-item').length;
        const randomIndexPlace = Math.floor(Math.random() * arrayLength);

        browser.pause(1000);

        $$('button.button.is-danger')[randomIndexPlace].click();
        const modalWindow = $('div.animation-content.modal-content');
        modalWindow.waitForDisplayed(10000);

        $('div.buttons.is-centered > button.button.is-danger').click();

        mainContainer.waitForDisplayed(10000);
    });
});