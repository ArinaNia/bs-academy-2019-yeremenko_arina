const assert = require('assert');
const {URL} = require('url');
const expect = require('chai').expect;

function bubbleClick(bubble) {
    for (let i = 0; i < bubble.length; i++) {
        bubble[i].click()
    }
}

describe('bubble game', () => {
    beforeEach(function() {
            browser.reloadSession();
            browser.url('https://task1-bvckdxdkxw.now.sh');
            let mainElement = $('center');
            mainElement.waitForDisplayed();
        });

    it('task_2_1 5s', () => {

            browser.pause(5000);

            let bubble = $$('.bubble');
            bubbleClick(bubble);

            let scoreGame = $('#score');
            scoreGame.waitForDisplayed();
            expect(scoreGame.getText()).to.equal(bubble.length.toString());
            //assert.equal(scoreGame.getText(), bubble.length);
        });
    it('task_2_1 inf', () => {
        //let bubble = $$('.bubble');
        browser.pause(1000);
        let temp = 0;
        let scoreGame = $('#score');

        while(1) {
            for (let i = 0; i < $$('.bubble').length; i++) {
                $$('.bubble')[i].click();
                temp += 1;
                expect(scoreGame.getText()).to.equal(temp.toString());
            }
        }
    });
});